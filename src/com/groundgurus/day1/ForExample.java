package com.groundgurus.day1;

public class ForExample {

    public static void main(String[] args) {
        String days[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
        
        // traditional for loop Java 4
        for (int i = days.length - 1; i >= 0; i--) {
            System.out.println(days[i]);
        }

        System.out.println("--------------");

        // enhanced for loop Java 5 and higher
        for (String day : days) {
            System.out.println(day);
        }
    }
}
