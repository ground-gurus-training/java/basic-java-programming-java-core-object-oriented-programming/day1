package com.groundgurus.day1;


public class Operators {
    public static void main(String[] args) {
        int id = 1000;
        int newId = ++id; // prefix increment
        
        System.out.println(id); // 1001
        System.out.println(newId); // 1001
        
        // 0000 1100 = 12
        // <<      2
        // ---------
        // 0011 0000 = 16 + 32

        int someValue = 12;
        int result = someValue << 2; // shift the binary to the left by 2

        System.out.println(result); // ?
        
        // Bitwise Operators
        
        //    0000 1011 = 11
        // |  0001 0101 = 21 (OR)
        // -----------------
        //    0001 1111 = 31
        
        int anotherValue = 11;
        int evenAnotherValue = 21;
        
        int result2 = anotherValue | evenAnotherValue; // ?
        System.out.println(result2);
    }
}
