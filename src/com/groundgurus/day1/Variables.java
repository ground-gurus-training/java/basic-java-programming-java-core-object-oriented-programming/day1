package com.groundgurus.day1;


public class Variables {
    public static void main(String[] args) {
        byte age = 32; // 8 bits, -2^7 to 2^7 - 1, -128 to 127
        int quantity = 129; // 32 bits, -2^31 to 2^31 - 1

        age = (byte) quantity; // casting from int to byte

        System.out.println(age);
        
        char status = '\u0096';
        System.out.println(status);
        
        // mutable = value can be changed
        // immutable = value cannot be changed

        // String in Java is immutable
        String firstName = "John"; // Reference Type/Class Type
        String lastName = "Doe";
        String fullName = firstName + " " + lastName;

        fullName = fullName.toUpperCase();

        System.out.println(fullName);
        System.out.println(fullName.toLowerCase());
    }
}
