package com.groundgurus.day1.exercises;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Day1Exercise2 {
    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ssa");
        System.out.println("Today is " + sdf.format(new Date()));
    }
}
