package com.groundgurus.day1.exercises;

import java.util.Scanner;


public class Day1Exercise1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Enter first number: ");
        int firstNumber = input.nextInt();
        
        System.out.print("Enter second number: ");
        int secondNumber = input.nextInt();
        
        int sum = firstNumber + secondNumber;
        System.out.println("The sum is " + sum);
        
        int difference = firstNumber - secondNumber;
        System.out.println("The difference is " + difference);
        
        int product = firstNumber * secondNumber;
        System.out.println("The product is " + product);
        
        int quotient = firstNumber / secondNumber;
        System.out.println("The quotient is " + sum);
        
        input.close();
    }
}
