package com.groundgurus.day1.exercises;

import java.util.Scanner;


public class Day1Exercise3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Enter first number: ");
        int firstNumber = input.nextInt();
        
        System.out.print("Enter second number: ");
        int secondNumber = input.nextInt();
        
        if (firstNumber > secondNumber) {
            System.out.println(firstNumber + " is greater than " + secondNumber);
        }
    }
}
