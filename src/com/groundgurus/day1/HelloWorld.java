package com.groundgurus.day1;

import static java.lang.System.out; // static imports introduction in Java 5
import java.util.Date;

public class HelloWorld {
    public static void main(String[] args) {
        out.println("Hello World");
        out.println(new Date());
    }
}
